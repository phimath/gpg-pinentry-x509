// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System.IO;

namespace Phimath.GPG.PinEntry.X509
{
    internal static class MagicNumbers
    {
        public static readonly byte[] FileIdentifier = { 0xAF, 0xFE, 0x00, 0xFF };

        public static readonly byte[] Delimiter = { 0xFF, 0x01, 0x00, 0x1F };

        public static void WriteMagicNumber(this BinaryWriter binaryStream)
        {
            binaryStream.Write(FileIdentifier);
        }

        public static void WriteDelimiter(this BinaryWriter binaryStream)
        {
            binaryStream.Write(Delimiter);
        }

        public static void ReadMagicNumberOrThrow(this BinaryReader binaryStream)
        {
            for (var i = 0; i < FileIdentifier.Length; i++)
            {
                var separatorByte = binaryStream.ReadByte();
                if (separatorByte != FileIdentifier[i])
                {
                    throw new InvalidDataException("Invalid file identifier");
                }
            }
        }

        public static void ReadDelimiterOrThrow(this BinaryReader binaryStream)
        {
            for (var i = 0; i < Delimiter.Length; i++)
            {
                var separatorByte = binaryStream.ReadByte();
                if (separatorByte != Delimiter[i])
                {
                    throw new InvalidDataException("Invalid delimiter");
                }
            }
        }
    }
}
