﻿#pragma warning disable SA1124

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// ReSharper disable HeapView.BoxingAllocation
// ReSharper disable HeapView.ObjectAllocation.Evident
// ReSharper disable HeapView.ObjectAllocation

namespace Phimath.GPG.PinEntry.X509
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var arguments = new List<string>(args.Length);
            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                var amILast = i == args.Length - 1;

                // ignore all those
                if (arg.StartsWith("--"))
                {
                    if (!amILast && !args[i + 1].StartsWith("--"))
                    {
                        i++;
                    }

                    continue;
                }

                arguments.Add(arg);
            }

            StreamWriter? log = null;
            try
            {
                var logPath = Path.Join(Path.GetTempPath(), "/gpg.pinentry.logs/");
                var logFileName = Path.Join(logPath, $"{DateTime.Now:yyyy-MM-dd-HH-mm-ss}");
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                else
                {
                    if (File.Exists($"{logFileName}.{LogExt}"))
                    {
                        logFileName += "-" + Path.GetRandomFileName();
                    }
                }

                log = File.CreateText($"{logFileName}.{LogExt}");
                log.WriteLine($"Version: {Version.ToString(3)}");
                log.WriteLine($"Args: {string.Join(" ", arguments)}");

                if (Environment.OSVersion.Platform != PlatformID.Win32NT)
                {
                    throw new PlatformNotSupportedException("Can only support Windows NT-based systems. Sorry.");
                }

                log.WriteLine("Environment okay");
                log.Flush();

                Store.Open(OpenFlags.ReadOnly);

                log.WriteLine("Store opened");
                log.WriteLine($"Base: {BasePath}, Secrets: {SecretsPath}");
                log.Flush();

                if (BasePath == null || SecretsPath == null)
                {
                    throw new ArgumentNullException(nameof(BasePath), "Something went wrong with the base path...");
                }

                log.WriteLine("Path okay");
                log.Flush();

                Environment.CurrentDirectory = BasePath;

                if (arguments.Count == 0)
                {
                    log.WriteLine("Agent mode");
                    log.Flush();
                    AssertsExists();
                    Read(
                        log,
                        out var cert,
                        out var aesKey,
                        out var activePasswords,
                        out var newPasswords,
                        out var lastRequestedPassword
                    );

                    log.WriteLine($"Found {activePasswords.Count} key(s)");
                    if (lastRequestedPassword != null)
                    {
                        log.WriteLine($"ALERT: Password change active for {lastRequestedPassword}");
                    }

                    log.Flush();

                    Console.WriteLine("OK Ready.");

                    string? touchFile = null;
                    string[]? fallbackIds = null;
                    var isPassphraseChange = lastRequestedPassword != null;

                    var lineCtr = 0;
                    string? line;
                    while ((line = Console.ReadLine()) != null)
                    {
                        log.WriteLine($"Line {lineCtr++}: {line}");
                        log.Flush();

                        var parseCommand = line.Split(" ", 2);
                        var command = parseCommand[0].Trim();
                        var value = parseCommand.Length > 1 ? parseCommand[1].Trim() : string.Empty;

                        switch (command)
                        {
                            case "OPTION":
                            {
                                var parsedOption = value.Split("=", 2);
                                var option = parsedOption[0];
                                var optionValue = parsedOption.Length > 1 ? parsedOption[1] : string.Empty;
                                switch (option)
                                {
                                    case "touch-file":
                                        touchFile = Path.GetFullPath(optionValue);
                                        break;
                                }

                                break;
                            }

                            case "GETINFO":
                            {
                                switch (value)
                                {
                                    case "flavor":
                                        Console.WriteLine("D Phimath.GPG.PinEntry.X509");
                                        break;
                                    case "version":
                                        var assembly = Assembly.GetExecutingAssembly();
                                        var v = assembly.GetName().Version ?? new Version(0, 0, 0);
                                        Console.WriteLine($"D {v.Major}.{v.Minor}.{v.Build}");
                                        break;
                                    case "pid":
                                        Console.WriteLine($"D {Process.GetCurrentProcess().Id}");
                                        break;
                                    case "ttyinfo":
                                        break;
                                }

                                break;
                            }

                            case "SETKEYINFO":
                            {
                                // Ignore
                                break;
                            }

                            case "SETQUALITYBAR":
                            case "SETQUALITYBAR_TT":
                            case "SETREPEATERROR":
                            case "SETREPEAT":
                            {
                                isPassphraseChange = true;
                                break;
                            }

                            case "SETDESC":
                            {
                                var matches = Regex.Matches(value, "[A-Z0-9]{16}|[a-z0-9]{16}");
                                fallbackIds = matches.Where(m => m.Success)
                                                     .Select(m => m.Value.ToUpperInvariant())
                                                     .Reverse()
                                                     .ToArray();
                                isPassphraseChange = isPassphraseChange || !matches.Any();

                                break;
                            }

                            case "GETPIN":
                            {
                                var password = activePasswords.FirstOrDefault().Value;

                                if (fallbackIds != null && fallbackIds.Length > 0)
                                {
                                    foreach (var fallbackId in fallbackIds)
                                    {
                                        if (!activePasswords.TryGetValue(fallbackId, out var tmp))
                                        {
                                            continue;
                                        }

                                        lastRequestedPassword ??= fallbackId;

                                        password = tmp;

                                        break;
                                    }
                                }
                                else if (isPassphraseChange)
                                {
                                    if (lastRequestedPassword == null)
                                    {
                                        log.Write("lastRequestedPassword == null");
                                        log.Flush();
                                        return;
                                    }

                                    if (!newPasswords.TryGetValue(lastRequestedPassword, out var tmp))
                                    {
                                        continue;
                                    }

                                    activePasswords[lastRequestedPassword] = tmp;
                                    newPasswords.Remove(lastRequestedPassword);
                                    password = tmp;
                                    Save(log, cert, aesKey, activePasswords, newPasswords, lastRequestedPassword);
                                }

                                if (touchFile != null)
                                {
                                    File.SetLastWriteTime(touchFile, DateTime.Now);
                                }
#if UNSAFE
                                log.WriteLine($"password: {password}");
                                log.Flush();
#endif
                                Console.WriteLine($"D {password}");
                                break;
                            }
                        }

                        Console.WriteLine("OK");
                    }
                    Save(log, cert, aesKey, activePasswords, newPasswords, lastRequestedPassword);

                    return;
                }

                switch (arguments[0])
                {
                    case "init":
                    {
                        if (File.Exists(SecretsPath))
                        {
                            Console.WriteLine("File already exists. Overwrite? (y/n)");
                            ConsoleKeyInfo key;
                            do
                            {
                                key = Console.ReadKey(false);
                            }
                            while (key.Key != ConsoleKey.Y && key.Key != ConsoleKey.N);

                            Console.WriteLine();

                            if (key.Key == ConsoleKey.N)
                            {
                                Console.WriteLine("Okay, not doing anything. Exiting now.");
                                return;
                            }
                        }

                        var thumb = arguments[1].ToUpperInvariant();
                        var cert = Cert(thumb);

                        Aes aes = new AesCng { KeySize = 256 };
                        aes.GenerateKey();
                        aes.GenerateIV();

                        WriteFile(cert, aes.Key, DataObject.Empty);
                        Console.WriteLine($"Initialized with certificate {thumb} (DN: {cert.SubjectName.Name})");
                        break;
                    }

                    case "upgrade":
                    {
#if RELEASE
                        Console.Error.WriteLine("Sorry, this feature is not available anymore!");
                        Console.Error.WriteLine(
                            "Please use the `view` command to extract your GPG passwords if you do not have them stored in another place."
                        );
                        Console.Error.WriteLine(
                            "If you are using version 3.0+ then you should not have to worry about upgrades anymore."
                        );
                        break;
#endif

                        AssertsExists();

                        Read(
                            log,
                            out var cert,
                            out var aesKey,
                            out var passwords,
                            out var changedPasswords,
                            out var lastRequestedPassword,
                            true
                        );

                        Save(log, cert, aesKey, passwords, changedPasswords, lastRequestedPassword);
                        Console.WriteLine("Success: Forced database upgrade.");

                        break;
                    }

                    case "reinit":
                    {
                        AssertsExists();
                        Read(
                            log,
                            out _,
                            out var aesKey,
                            out var passwords,
                            out var changedPasswords,
                            out var lastRequestedPassword
                        );
                        var thumb = arguments[1].ToUpperInvariant();
                        var cert = Cert(thumb);
                        Save(log, cert, aesKey, passwords, changedPasswords, lastRequestedPassword);
                        Console.WriteLine($"Chanced to new certificate {thumb} (DN: {cert.SubjectName.Name})");
                        break;
                    }

                    case "list":
                    {
                        AssertsExists();
                        Read(
                            log,
                            out var cert,
                            out _,
                            out var dict,
                            out var changedPasswords,
                            out var lastRequestedPassword
                        );

                        Console.WriteLine(
                            "Store is encrypted with certificate {0} (Thumbprint {1})",
                            cert.SubjectName.Name,
                            cert.Thumbprint!.ToUpperInvariant()
                        );

                        var keys = dict.Keys;
                        if (keys.Count == 0)
                        {
                            Console.WriteLine("No GPG Key IDs in this store");
                        }
                        else
                        {
                            Console.WriteLine($"GPG Key IDs in this store [{keys.Count} total]:");

                            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
                            foreach (var id in keys)
                            {
                                var formattedId = string.Join(
                                    ' ',
                                    id.Select((c, idx) => (c, idx))
                                      .GroupBy(g => Math.Floor(g.idx / 4d), tuple => tuple.c)
                                      .Select(g => string.Join(string.Empty, g))
                                );
                                Console.WriteLine($"- {formattedId}");
                            }
                        }

                        Console.WriteLine($"INFO: Passphrase change requested for {changedPasswords.Count} key(s).");
                        Console.WriteLine($"INFO: Last requested key was {lastRequestedPassword ?? "<none>"}.");

                        break;
                    }

                    case "view":
                    {
                        AssertsExists();
                        Read(
                            log,
                            out var cert,
                            out _,
                            out var data,
                            out var changedPasswords,
                            out var lastRequestedPassword
                        );

                        Console.WriteLine(
                            "Store is encrypted with certificate {0} (Thumbprint {1})",
                            cert.SubjectName.Name,
                            cert.Thumbprint!.ToUpperInvariant()
                        );

                        if (data.Count == 0)
                        {
                            Console.WriteLine("No GPG Key IDs in this store");
                        }
                        else
                        {
                            Console.WriteLine($"GPG Key IDs in this store [{data.Count} total]:");
                            var index = 0;
                            var noFormatLen = data.Count.ToString().Length;
                            var format = "[{0," + noFormatLen + "}] {1}";
                            var list = new string[data.Count];
                            foreach (var (key, value) in data)
                            {
                                var formattedId = string.Join(
                                    ' ',
                                    key.Select((c, idx) => (c, idx))
                                       .GroupBy(g => Math.Floor(g.idx / 4d), tuple => tuple.c)
                                       .Select(g => string.Join(string.Empty, g))
                                );
                                Console.WriteLine(format, index, formattedId);
                                list[index] = value;
                                index++;
                            }

                            Console.WriteLine(
                                "--------------------------------------------------------------------------------"
                            );
                            Console.WriteLine("NOTICE: CARE PASSWORD WILL BE SHOWN IN CLEAR TEXT!!!");
                            Console.Write($"Which key you want to show? [0 .. {data.Count - 1}]:  ");
                            var answer = Console.ReadLine()?.Trim();
                            if (!int.TryParse(answer, out var number))
                            {
                                Console.Error.WriteLine("Invalid format: You need to enter a number!");
                                return;
                            }

                            if (number < 0 || number >= data.Count)
                            {
                                Console.Error.WriteLine("Invalid input: Input out of bounds.");
                                return;
                            }

                            Console.WriteLine(
                                "+-------------------------------------------------------------------------------"
                            );
                            Console.WriteLine("| {0,-76} |", list[number]);
                            Console.WriteLine(
                                "+-------------------------------------------------------------------------------"
                            );
                        }

                        Console.WriteLine($"INFO: Passphrase change requested for {changedPasswords.Count} key(s).");
                        Console.WriteLine($"INFO: Last requested key was {lastRequestedPassword ?? "<none>"}.");

                        break;
                    }

                    case "add":
                    case "edit":
                    case "remove":
                    {
                        AssertsExists();
                        Read(
                            log,
                            out var cert,
                            out var aesKey,
                            out var passwords,
                            out var changedPasswords,
                            out var lastRequestedPassword
                        );

                        // TODO: deny if operation is ongoing
                        var normalizedId = string.Join(string.Empty, arguments.Skip(1)).ToUpperInvariant();
                        log?.WriteLine($"Processing normalized Key ID {normalizedId}");
                        switch (arguments[0])
                        {
                            case "add":
                            {
                                if (passwords.ContainsKey(normalizedId))
                                {
                                    Console.WriteLine($"GPG key id {normalizedId} already exists. Overwrite? (y/n)");
                                    ConsoleKeyInfo key;
                                    do
                                    {
                                        key = Console.ReadKey(false);
                                    }
                                    while (key.Key != ConsoleKey.Y && key.Key != ConsoleKey.N);

                                    Console.WriteLine();

                                    if (key.Key == ConsoleKey.N)
                                    {
                                        Console.WriteLine("Okay, not doing anything. Exiting now.");
                                        return;
                                    }
                                }

                                Console.WriteLine("Please enter the password you want to save now");
                                passwords[normalizedId] = ReadPassword();
                                Save(log, cert, aesKey, passwords, changedPasswords, lastRequestedPassword);
                                Console.WriteLine($"Saved password for GPG key id {normalizedId}.");
                                break;
                            }

                            case "edit":
                            {
                                if (changedPasswords.ContainsKey(normalizedId))
                                {
                                    Console.WriteLine(
                                        $"GPG key id {normalizedId} password change already registered. Overwrite? (y/n)"
                                    );
                                    ConsoleKeyInfo key;
                                    do
                                    {
                                        key = Console.ReadKey(false);
                                    }
                                    while (key.Key != ConsoleKey.Y && key.Key != ConsoleKey.N);

                                    Console.WriteLine();

                                    if (key.Key == ConsoleKey.N)
                                    {
                                        Console.WriteLine("Okay, not doing anything. Exiting now.");
                                        return;
                                    }
                                }

                                Console.WriteLine("Please enter the new password you want to save now");
                                changedPasswords[normalizedId] = ReadPassword();
                                Save(log, cert, aesKey, passwords, changedPasswords, lastRequestedPassword);
                                Console.WriteLine($"Saved password change for GPG key id {normalizedId}.");
                                break;
                            }

                            case "remove":
                            {
                                if (!passwords.Remove(normalizedId))
                                {
                                    Console.WriteLine("Did not find GPG key id, so nothing happened.");
                                    return;
                                }

                                Console.WriteLine($"Removed password for GPG key id {normalizedId}.");
                                Save(log, cert, aesKey, passwords, changedPasswords, lastRequestedPassword);
                                break;
                            }
                        }

                        break;
                    }

                    default:
                    {
                        Console.WriteLine("Usage:");
                        Console.WriteLine(
                            "                                                                                "
                        );
                        Console.WriteLine(
                            "  upgrade                            | Converts the store to the current version"
                        );
                        Console.WriteLine(
                            "                                                                                "
                        );
                        Console.WriteLine(
                            "  init <CertificateThumbPrint>       | Initializes the store with a new cert    "
                        );
                        Console.WriteLine(
                            "  reinit <NewCertificateThumbPrint>  | Re-encrypts the store with another cert  "
                        );
                        Console.WriteLine(
                            "                                                                                "
                        );
                        Console.WriteLine(
                            "  add <PgpKeyId>                     | Adds the PGP key ID to the store         "
                        );
                        Console.WriteLine(
                            "  edit <PgpKeyId>                    | Change the password for the PGP key ID   "
                        );
                        Console.WriteLine(
                            "  remove <PgpKeyId>                  | Removed the PGP key ID from the store    "
                        );
                        Console.WriteLine(
                            "                                                                                "
                        );
                        Console.WriteLine(
                            "  list                               | Views all saved PGP key IDs              "
                        );
                        Console.WriteLine(
                            "  view                               | Views the saved password                 "
                        );
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                if (log != null)
                {
                    log.WriteLine($"ERROR: {e.Message}");
                    log.WriteLine(e.StackTrace);
                }
                else
                {
                    Console.Error.WriteLine($"ERROR: {e.Message}");
                    Console.Error.WriteLine(e.StackTrace);
                }
            }
            finally
            {
                log?.Flush();
                log?.Close();
            }
        }

        private static string ReadPassword()
        {
            var pw = new StringBuilder();
            do
            {
                var key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pw.Append(key.KeyChar);
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pw.Length > 0)
                    {
                        pw.Remove(pw.Length - 1, 1);
                        Console.Write("\b \b");
                    }
                    else if (key.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine();
                        break;
                    }
                }
            }
            while (true);

            return pw.ToString();
        }

        private static X509Certificate2 Cert(string thumbprint)
        {
            var certs = Store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
            if (certs.Count == 0)
            {
                throw new ApplicationException("Did not find a matching certificate");
            }

            return certs[0];
        }

        private static void AssertsExists()
        {
            if (!File.Exists(SecretsPath))
            {
                throw new ArgumentException(
                    $"Make sure the file '{SecretsPath}' exists and is accessible! You need to store your secrets there"
                );
            }
        }

        #region Constants

        private const string LogExt = "log";
        private static readonly Encoding Encoding = Encoding.UTF8;
        private static readonly RSAEncryptionPadding Padding = RSAEncryptionPadding.Pkcs1;
        private static readonly Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static readonly Version Version = Assembly.GetName().Version!;
        private static readonly string? BasePath = Path.GetDirectoryName(Assembly.Location);

        private static readonly string? SecretsPath = Path.Join(
            Environment.GetEnvironmentVariable("PINENTRY_BASE", EnvironmentVariableTarget.Process)
            ?? Environment.GetEnvironmentVariable("PINENTRY_BASE", EnvironmentVariableTarget.User) ?? BasePath,
            "gpg.keys"
        );

        private static readonly X509Store Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

        #endregion

        #region Encrypted Read & Write

        private static void WriteFile(X509Certificate2 cert, byte[] key, DataObject data)
        {
            var content = Encoding.GetBytes(JsonConvert.SerializeObject(data));

            var certBytes = cert.GetCertHash();

            // Create AES with the given key data
            Aes aes = new AesCng();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = key;
            aes.GenerateIV();

            // Encrypt AES data
            using var rsa = cert.GetRSAPublicKey();
            key = rsa.Encrypt(key, Padding);

            // Get the output stream
            using var fileStream = File.Create(SecretsPath);
            using var binaryStream = new BinaryWriter(fileStream);

            // Write magic number
            binaryStream.WriteMagicNumber();

            // Write thumbprint
            binaryStream.Write(certBytes.Length);
            binaryStream.Write(certBytes);

            // Write delimiter
            binaryStream.WriteDelimiter();

            // Write IV length and data
            binaryStream.Write(aes.IV.Length);
            binaryStream.Write(aes.IV);

            // Write delimiter
            binaryStream.WriteDelimiter();

            // Write key length and data
            binaryStream.Write(key.Length);
            binaryStream.Write(key);

            // Write delimiter
            binaryStream.WriteDelimiter();

            // Write serialized data
            var minimumBlockSize = aes.BlockSize / 8;
            var contentLength = content.Length;
            var contentBlocks = contentLength / minimumBlockSize;
            var modContentLength = contentLength % minimumBlockSize;

            var paddedContentLength = contentBlocks * minimumBlockSize;
            if (modContentLength != 0)
            {
                paddedContentLength += minimumBlockSize;
            }

            binaryStream.Write(paddedContentLength);

            var positionBefore = fileStream.Position;

            using var cryptoStream = new CryptoStream(
                binaryStream.BaseStream,
                aes.CreateEncryptor(),
                CryptoStreamMode.Write
            );
            cryptoStream.Write(content);
            cryptoStream.FlushFinalBlock();

            var positionAfter = fileStream.Position;

            var deltaPos = positionAfter - positionBefore;

            // Write delimiter
            binaryStream.WriteDelimiter();

            binaryStream.Flush();
        }

        private static void ReadFile(
            StreamWriter? log,
            out string thumbprint,
            out X509Certificate2 cert,
            out byte[] aesKey,
            out byte[] content
        )
        {
            log?.WriteLine("Opening secrets");
            using var fileStream = File.OpenRead(SecretsPath);
            using var binaryStream = new BinaryReader(fileStream);

            binaryStream.ReadMagicNumberOrThrow();
            log?.WriteLine("Magic number present");

            var thumbprintLength = binaryStream.ReadInt32();
            var thumbPrintData = binaryStream.ReadBytes(thumbprintLength);

            thumbprint = thumbPrintData.ToHexStringUpper();
            cert = Cert(thumbprint);

            binaryStream.ReadDelimiterOrThrow();
            log?.WriteLine("Delimiter present");

            var ivLength = binaryStream.ReadInt32();
            var iv = binaryStream.ReadBytes(ivLength);

            binaryStream.ReadDelimiterOrThrow();
            log?.WriteLine("Delimiter present");

            var keyLength = binaryStream.ReadInt32();
            aesKey = binaryStream.ReadBytes(keyLength);

            binaryStream.ReadDelimiterOrThrow();
            log?.WriteLine("Delimiter present");

            using var rsa = cert.GetRSAPrivateKey();
            aesKey = rsa.Decrypt(aesKey, Padding);

            Aes aes = new AesCng { Key = aesKey, IV = iv };
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            var minimumBlockSize = aes.BlockSize / 8;

            var contentLength = binaryStream.ReadInt32();
            var contentBlocks = contentLength / minimumBlockSize;
            var modContentLength = contentLength % minimumBlockSize;

            var paddedContentLength = contentBlocks * minimumBlockSize;
            if (modContentLength != 0)
            {
                paddedContentLength += minimumBlockSize;
            }

            log?.WriteLine(
                "Trying decrypt with Length={0}, padded to {1}, for minimum block size {2}",
                contentLength,
                paddedContentLength,
                minimumBlockSize
            );

            using var decryptor = aes.CreateDecryptor();
            var xContent = binaryStream.ReadBytes(paddedContentLength);
            content = decryptor.TransformFinalBlock(xContent, 0, paddedContentLength);

            binaryStream.ReadDelimiterOrThrow();
            log?.WriteLine("EOF Delimiter present");
        }

        #endregion

        #region Strucuted Read & Write

        private static void Read(
            StreamWriter? log,
            out X509Certificate2 cert,
            out byte[] aesKey,
            out Dictionary<string, string> passwords,
            out Dictionary<string, string> changedPasswords,
            out string? lastRequestedPassword,
            bool ignoreVersion = false
        )
        {
            ReadFile(log, out var thumbprint, out cert, out aesKey, out var content);

            log?.WriteLine($"Read file with thumbprint {thumbprint} and content length of {content.Length} byte(s)");

            var token = JObject.Parse(Encoding.GetString(content));
            var version = Version.Parse(
                token.TryGetValue("_version", out var versionToken) ? versionToken.ToObject<string>()! : "1.0.0"
            );

            if (!ignoreVersion && version.Major != Version.Major)
            {
                throw new FileLoadException($"wrong database version: expected {Version.ToString(3)}, got {version}");
            }

            (passwords, changedPasswords, lastRequestedPassword) = token.ToObject<DataObject>()!;
        }

        private static void Save(
            StreamWriter? log,
            X509Certificate2 cert,
            byte[] key,
            Dictionary<string, string> passwords,
            Dictionary<string, string> changedPasswords,
            string? lastRequestedPassword
        )
        {
            log?.WriteLine(
                $"Saving {passwords.Count} password(s), of which {changedPasswords.Count} have been changed"
            );
            var data = new DataObject(passwords, changedPasswords, lastRequestedPassword);

            WriteFile(cert, key, data);
        }

        #endregion
    }
}
