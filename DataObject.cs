// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System.Collections.Generic;

using Newtonsoft.Json;

namespace Phimath.GPG.PinEntry.X509
{
    [JsonObject(MemberSerialization.Fields)]
    internal readonly struct DataObject
    {
        public static DataObject Empty =>
            new DataObject(new Dictionary<string, string>(), new Dictionary<string, string>(), null);

        [JsonProperty(Required = Required.Always)]
        internal readonly Dictionary<string, string> _passwords;

        [JsonProperty(Required = Required.Always, NullValueHandling = NullValueHandling.Include)]
        internal readonly Dictionary<string, string> _changedPasswords;

        [JsonProperty(Required = Required.AllowNull, NullValueHandling = NullValueHandling.Include)]
        internal readonly string? _lastRequestedPassword;

        [JsonProperty(Required = Required.Always)]
        internal readonly string _version;

        public DataObject(
            Dictionary<string, string> passwords,
            Dictionary<string, string> changedPasswords,
            string? lastRequestedPassword
        )
        {
            _passwords = passwords;
            _changedPasswords = changedPasswords;
            _lastRequestedPassword = lastRequestedPassword;
            _version = Program.Version.ToString(3);
        }

        public void Deconstruct(
            out Dictionary<string, string> passwords,
            out Dictionary<string, string> changedPasswords,
            out string? lastRequestedPassword
        )
        {
            passwords = _passwords;
            changedPasswords = _changedPasswords;
            lastRequestedPassword = _lastRequestedPassword;
        }
    }
}
