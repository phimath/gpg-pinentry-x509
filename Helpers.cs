// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

#pragma warning disable SA1611, SA1615

using System;

namespace Phimath.GPG.PinEntry.X509
{
    public static class Helpers
    {
        /// <summary>
        ///     Encode a byte array as an upper case hex string.
        /// </summary>
        public static string ToHexStringUpper(this byte[] bytes)
        {
            return string.Create(bytes.Length * 2, bytes, (chars, src) => ToHexArrayUpper(src, chars));
        }

        private static char NibbleToHex(byte b)
        {
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return (char)(b >= 0 && b <= 9 ? '0' + b : 'A' + (b - 10));
        }

        private static void ToHexArrayUpper(byte[] bytes, Span<char> chars)
        {
            var i = 0;
            foreach (var b in bytes)
            {
                chars[i++] = NibbleToHex((byte)(b >> 4));
                chars[i++] = NibbleToHex((byte)(b & 0xF));
            }
        }
    }
}
