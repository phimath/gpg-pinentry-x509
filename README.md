# GPG.PinEntry.X509

This tool is a replacement for the visual or console PIN entry module provided 
by GPG. It allows to store passwords to multiple PGP keys secured by a X509 
certificate and the Windows Certificate Store. This also allows the user to use 
smartcards.

Implementations using similar tools to the Windows Certificate Store for cross-
platform use are welcome. Just drop me an email at philipp@phimath.de.